package data;

import java.util.Objects;

public class Path {

	public int departureTime;
	public int arrivalTime;
	public String source;
	public String destination;
	public String name;
	public int cost;
	public String planeType;
	
	public Path(String depart, String arrive, String destination, String name, String cost, String planeType, String source) {
		this.departureTime = Integer.valueOf(depart).intValue();
		this.arrivalTime = Integer.valueOf(arrive).intValue();
		this.destination = destination;
		this.name = name;
		this.cost = Integer.valueOf(cost).intValue();
		this.planeType = planeType;
		this.source = source;
	}

	@Override
	public String toString() {
		return "Path [departureTime=" + departureTime + ", arrivalTime=" + arrivalTime + ", source=" + source
				+ ", destination=" + destination + ", name=" + name + ", cost=" + cost + ", planeType=" + planeType
				+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		Path p = (Path)obj;
		
		return Objects.equals(this.name, p.name);
	}
	
	
}
